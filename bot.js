const https = require("https");

var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

var forumInfoUrl = `https://www.lantredujeu.net/Forum/last_24h_feed.php?auth=${auth.token}`;
var started = false;
var timer; 
var generalChannelId;
var testChannelId;
var serverID
var roles;
var adminRoleID;


bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');

    serverID = Object.keys(bot.servers)[0];
    roles = bot.servers[serverID].roles;

    console.log(`Getting admin role id`)
    Object.values( roles ).some(function(role) {
        if (role.name === "@admin") {
            adminRoleID = role.id;
        }
    });

    console.log(`Getting forums ID`)
    Object.values(bot.channels).some(function(channel) {
        if( channel.name === "general" ) { generalChannelId = channel.id }
        if( channel.name === "test-bot" ) { testChannelId = channel.id }
    });
    console.log(`------------------`)
    console.log(`Bot is active !`)
});
bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        args = args.splice(1);
        switch(cmd) {
            // !ping
            // case 'ping':
            //     bot.sendMessage({
            //         to: channelID,
            //         message: 'Pong!'
            //     });
            // break;

            case 'start':
                console.log(`Command : Start`)
                if( !isAdmin(evt.d.member) ) { console.log(`Error : Not admin`); return };
                if(started){ console.log(`Error : Allready Started`); return }

                started = true;
                update( (1000*60*60*24) );
            break;

            case 'once':
                console.log(`Command : Once`)
                if( !isAdmin(evt.d.member) ) { console.log(`Error : Not admin`); return };

                getForumInfo(forumInfo => {
                    talk(forumInfo);
                });
            break;

            case 'stop':
                console.log(`Command : Stop`)
                if( !isAdmin(evt.d.member) ) { console.log(`Error : Not admin`); return };
                if(!started){ console.log(`Error : Not started`); return }

                bot.sendMessage({
                    to: channelID,
                    message: 'Ok chef !'
                });
                started = false;
                clearInterval(timer);

            break;
            case 'disconnect':
                console.log(`Command : disconnect`)
                if( !isAdmin(evt.d.member) ) { console.log(`Error : Not admin`); return };
                bot.sendMessage({
                    to: channelID,
                    message: 'Bye !'
                });
                bot.disconnect();
            break;
            // Just add any case commands if you want to..
         }
     }
});

bot.on('message', (user, userID, channelID, message, evt)=> {
    let mentions = evt.d.mentions;
    if(mentions.length == 0) { return }
    if( mentions.find( el => el.username ).username === bot.username ) {
        bot.sendMessage({
            to: channelID,
            message: `Bonjour, je suis un robot en cours de développement !`
        });
    }
});

let update = delay => {
    if( !started ) { return }
    getForumInfo(forumInfo => {
        talk(forumInfo);
        timer = setTimeout(function(){ update(delay) }, delay);
    });
}

let talk = forumInfo => {
    console.log(`Compose message : Started`);
    let message = "Oyez, Oyez !\n\n";

    if( forumInfo.newMessageNumber > 0 ) {
        message += `**Sur le forum, il y'a eu ${forumInfo.newMessageNumber} nouveau`;
        if( forumInfo.newMessageNumber > 1 ) {message += `x`; }
        message += ` message`;
        if( forumInfo.newMessageNumber > 1 ) {message += `s`; }
        message += ` ces dernières 24h :** \n`;
        message += `Voir les nouveaux messages : <https://www.lantredujeu.net/Forum/search.php?search_id=newposts>`
    }
    if( forumInfo.newTopicNumber > 0 ) {  
        message += `\n\n**Aujourd'hui, il y'a également ${forumInfo.newTopicNumber} nouveau`
        if( forumInfo.newTopicNumber > 1 ) {message += `x`; }
        message += ` sujet`;
        if( forumInfo.newTopicNumber > 1 ) {message += `s`; }
        message += ` :** \n`;

        for (var i = forumInfo.newTopics.length - 1; i >= 0; i--) {
            message += `${forumInfo.newTopics[i].topic_title} : <https://www.lantredujeu.net/Forum/viewtopic.php?f=${forumInfo.newTopics[i].forum_id}&t=${forumInfo.newTopics[i].topic_id}>\n`
        }
    }

    if( forumInfo.newMessageNumber > 0 || forumInfo.newTopicNumber > 0 ) {
        console.log(`Publish message`);
        bot.sendMessage({
            to: generalChannelId,
            message: message
        });
    } else {
        console.log(`Error : No post to display`);
    }
}
let isAdmin = member => {
    return member.roles.indexOf(adminRoleID) >= 0;
}

let getForumInfo = callback => {
    console.log(`Fetch : Started from ${forumInfoUrl}`);
    https.get(forumInfoUrl, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        console.log(`Fetch : Ended`);
        console.log(body);
        body = JSON.parse(body);
        callback(body);
      });
    });
}